#include <iostream>
#include <string.h>
using namespace std;
/*
Como entrada tenemos un arreglo char el cual contiene letras o una palabra
Como salida tenemos las letras o palabras pero todas en mayusculas
*/

int main()
{
    char Cadena_Caracteres[100];            //Arreglo de palabras bien sea mayusculas o minusculas
    int Valor_Mayuscula;
    cout<<"Ingrese su palabra para ser cambiada a mayusculas: \n";      //Ingresa la palabra no importa si algunas son mayusculas o minusculas
    cin>>Cadena_Caracteres;
    for(int i=0;Cadena_Caracteres[i]!='\0';i++){                  //Este ciclo se hace hasta que encuentre un salto de linea del arreglo que es la ultima posicion del arreglo hasta que se lleno
        if(Cadena_Caracteres[i]>=97 && Cadena_Caracteres[i]<=122){  //Condicion para confirmar si la letra es minuscula para pasarla a mayuscula
            Valor_Mayuscula=Cadena_Caracteres[i]-32;                //Se pasa a mayuscula
            Cadena_Caracteres[i]=Valor_Mayuscula;
        }

    }
    for(int i=0; Cadena_Caracteres[i]!='\0';i++){
        cout<<Cadena_Caracteres[i];
    }
cout<<endl;
    return 0;
}

